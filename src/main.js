import Vue from 'vue';
import App from './App.vue';
import vuetify from './plugins/vuetify';
import Vuelidate from 'vuelidate';
import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "./main.css";
import Amplify from 'aws-amplify';
import aws_exports from './aws-exports';
import {
  applyPolyfills,
  defineCustomElements,
} from '@aws-amplify/ui-components/loader';

Amplify.configure(aws_exports);
applyPolyfills().then(() => {
  defineCustomElements(window);
});

Vue.config.productionTip = false;
Vue.use(vuetify);
Vue.use(Vuelidate);

new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app');